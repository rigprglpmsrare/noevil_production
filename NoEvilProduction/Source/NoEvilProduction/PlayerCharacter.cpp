// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Engine.h"
#include "PuzzleBlockButton.h"
#include "UnrealNetwork.h"
#include "MazeEntranceTrigger.h"
#include "MazePressurePlate.h"
#include "MazeRestartTrigger.h"
#include "MazeExitKey.h"
#include "MazeExitDoor.h"
#include "MazeMap.h"
#include "Kismet/GameplayStatics.h"

APlayerCharacter::APlayerCharacter()
	: ColourModuleOn(true)
	, VisionModuleOn(true)
	, PlayerFocalDistance(5.0f)
	, ScreenVignetteIntensity(1.8f)
	, ButtonAvailableToPress(nullptr)
	, KeyAvailableToPickup(nullptr)
	, MazeEntranceEffect(false)
	, MazeEffectTrigger(false)
{
	PrimaryActorTick.bCanEverTick = false;

	bReplicateMovement = true;
	bReplicates = true;

	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Player Camera"));
	PlayerCamera->SetupAttachment(GetCapsuleComponent());

	PlayerCamera->SetRelativeLocation(FVector(0.0f, 0.0f, 25.0f + BaseEyeHeight));
	PlayerCamera->bUsePawnControlRotation = true;

	InteractableSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Interactables Sphere"));
	InteractableSphere->InitSphereRadius(90.0f);
	InteractableSphere->SetCollisionProfileName("InteractablesTrigger");
	InteractableSphere->SetupAttachment(GetCapsuleComponent());

	InteractableSphere->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::OnOverlapBegin);
	InteractableSphere->OnComponentEndOverlap.AddDynamic(this, &APlayerCharacter::OnOverlapEnd);
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APlayerCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APlayerCharacter::AddControllerPitchInput);

	PlayerInputComponent->BindAction("ColourModuleToggle", IE_Pressed, this, &APlayerCharacter::ColourModuleToggle);
	PlayerInputComponent->BindAction("VisionModuleToggle", IE_Pressed, this, &APlayerCharacter::VisionModuleToggle);

	PlayerInputComponent->BindAction("Interactions", IE_Pressed, this, &APlayerCharacter::AvailableInteractions);
}

void APlayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerCharacter, ButtonAvailableToPress);
}

void APlayerCharacter::MoveForward(float ScaleValue)
{
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
	AddMovementInput(Direction, ScaleValue);
}

void APlayerCharacter::MoveRight(float ScaleValue)
{
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(Direction, ScaleValue);
}

void APlayerCharacter::AvailableInteractions_Implementation()
{
	if (ButtonAvailableToPress)
	{
		ButtonPressSound();
		ButtonAvailableToPress->AssignedPuzzleBlock->ChangeLocation_Implementation();
	}

	if (KeyAvailableToPickup)
	{
		AddKeyToInventory();
		KeyAvailableToPickup->PickUp();
	}
}

void APlayerCharacter::AddKeyToInventory_Implementation()
{
	KeyAvailableToPickup->AddToInventory(PlayerInventory);
}

void APlayerCharacter::ButtonPressSound_Implementation()
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ButtonAvailableToPress->PressedSound, ButtonAvailableToPress->GetActorLocation());
}

void APlayerCharacter::ColourModuleToggle_Implementation()
{
	if (ColourModuleOn)
	{
		PlayerCamera->PostProcessSettings.ColorSaturation = FVector4(0.0f, 0.0f, 0.0f, 0.0f);
		if (MazeNavigationMap)
		{
			MazeNavigationMap->ChangeToColourOffMaterial();
		}
		ColourModuleOn = false;
	}
	else
	{
		PlayerCamera->PostProcessSettings.ColorSaturation = FVector4(1.0f, 1.0f, 1.0f, 1.0f);
		if (MazeNavigationMap)
		{
			MazeNavigationMap->ChangeToColourOnMaterial();
		}
		ColourModuleOn = true;
	}
}

void APlayerCharacter::VisionModuleToggle_Implementation()
{
	if (VisionModuleOn)
	{
		MazeEntranceEffect = false;
		MazeEntranceEffectToggle_Implementation();

		PlayerCamera->PostProcessSettings.DepthOfFieldFocalDistance = PlayerFocalDistance;
		PlayerCamera->PostProcessSettings.VignetteIntensity = ScreenVignetteIntensity;
		VisionModuleOn = false;
	}
	else
	{
		if (MazeEffectTrigger) 
		{
			MazeEntranceEffect = true;
			MazeEntranceEffectToggle_Implementation();
		}
		
		PlayerCamera->PostProcessSettings.DepthOfFieldFocalDistance = 0.0f;
		PlayerCamera->PostProcessSettings.VignetteIntensity = 0.0f;
		VisionModuleOn = true;
	}
}

void APlayerCharacter::MazeEntranceEffectToggle_Implementation()
{
	if (MazeEntranceEffect)
	{
		/*PlayerCamera->PostProcessSettings.BloomIntensity = 8.0f;

		PlayerCamera->PostProcessSettings.AutoExposureBias = 2.5f;
		PlayerCamera->PostProcessSettings.AutoExposureMaxBrightness = 100.0f;

		PlayerCamera->PostProcessSettings.ChromaticAberrationStartOffset = 0.0f;
		PlayerCamera->PostProcessSettings.SceneFringeIntensity = 5.0f;

		PlayerCamera->PostProcessSettings.RayTracingReflectionsMaxBounces = 4.0f;

		PlayerCamera->PostProcessSettings.MotionBlurAmount = 1.0f;
		PlayerCamera->PostProcessSettings.MotionBlurMax = 50.0f;*/

		PlayerCamera->PostProcessSettings.ColorGamma = FVector(0.0f, 0.0f, 0.0f);
	}
	else
	{
		PlayerCamera->PostProcessSettings.ColorGamma = FVector(1.0f, 1.0f, 1.0f);

		/*PlayerCamera->PostProcessSettings.BloomIntensity = 0.675f;

		PlayerCamera->PostProcessSettings.AutoExposureBias = 0.0f;
		PlayerCamera->PostProcessSettings.AutoExposureMaxBrightness = 20.0f;

		PlayerCamera->PostProcessSettings.ChromaticAberrationStartOffset = 0.0f;
		PlayerCamera->PostProcessSettings.SceneFringeIntensity = 0.0f;

		PlayerCamera->PostProcessSettings.RayTracingReflectionsMaxBounces = 1.0f;

		PlayerCamera->PostProcessSettings.MotionBlurAmount = 0.5f;
		PlayerCamera->PostProcessSettings.MotionBlurMax = 5.0f;*/
	}
}

void APlayerCharacter::MazeRestart_Implementation()
{
	SetActorLocation(FVector(856.6f, -957.0f, 40.0f));
}

void APlayerCharacter::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != this && OtherActor->GetClass()->IsChildOf(AMazeExitKey::StaticClass()))
	{
		KeyAvailableToPickup = Cast<AMazeExitKey>(OtherActor);
	}

	if (OtherActor != this && OtherActor->GetClass()->IsChildOf(AMazeMap::StaticClass()))
	{
		MazeNavigationMap = Cast<AMazeMap>(OtherActor);
	}

	else if (OtherActor != this && OtherActor->GetClass()->IsChildOf(APuzzleBlockButton::StaticClass()))
	{
		/*if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Blue, TEXT("Boop! Button"));
		}*/

		ButtonAvailableToPress = Cast<APuzzleBlockButton>(OtherActor);
	}

	else if (OtherActor != this && OtherActor->GetClass()->IsChildOf(AMazeEntranceTrigger::StaticClass()))
	{
		//if (GEngine)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Blue, TEXT("Triggered"));
			MazeEntranceEffect = true;
			MazeEffectTrigger = true;
			MazeEntranceEffectToggle();
		}
	}
	
	else if (OtherActor != this && OtherActor->GetClass()->IsChildOf(AMazePressurePlate::StaticClass()))
	{
		AMazePressurePlate* CurrentPressurePlate = Cast<AMazePressurePlate>(OtherActor);

		CurrentPressurePlate->Activated();
	}

	else if (OtherActor != this && OtherActor->GetClass()->IsChildOf(AMazeRestartTrigger::StaticClass()))
	{
		MazeRestart();
	}

	else if (OtherActor != this && OtherActor->GetClass()->IsChildOf(AMazeExitDoor::StaticClass()))
	{
		MazeExitDoor = Cast<AMazeExitDoor>(OtherActor);

		if (MazeExitDoor && PlayerInventory.FindByKey("ExitKey"))
		{
			MazeExitDoor->ShouldOpen();
		}
		
	}
}

void APlayerCharacter::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor != this && OtherActor->GetClass()->IsChildOf(AMazeExitKey::StaticClass()))
	{
		KeyAvailableToPickup = nullptr;
	}

	else if (OtherActor != this && OtherActor->GetClass()->IsChildOf(APuzzleBlockButton::StaticClass()))
	{

		ButtonAvailableToPress = nullptr;
	}

	else if (OtherActor != this && OtherActor->GetClass()->IsChildOf(AMazePressurePlate::StaticClass()))
	{
		AMazePressurePlate* CurrentPressurePlate = Cast<AMazePressurePlate>(OtherActor);

		CurrentPressurePlate->Deactivated();
	}
}