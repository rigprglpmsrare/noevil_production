// Fill out your copyright notice in the Description page of Project Settings.


#include "MazeExitDoor.h"
#include "Components/BoxComponent.h"

AMazeExitDoor::AMazeExitDoor()
	: ExitDoorTrigger(CreateDefaultSubobject<UBoxComponent>(TEXT("Exit Door Trigger")))
{
	RootComponent = ExitDoorTrigger;

	DoorMesh->SetupAttachment(RootComponent);

	PrimaryActorTick.bCanEverTick = true;
}

void AMazeExitDoor::BeginPlay()
{
	DoorOpenLocation = DoorOpenActor->GetActorLocation();
}

void AMazeExitDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsOpening)
	{
		
	}
}

void AMazeExitDoor::ShouldOpen()
{
	if (!IsOpening)
	{
		IsOpening = true;
	}

	if (IsOpening)
	{
		float DoorLerp = 0.0f;

		while(DoorLerp <= 1.0f)
		{
			SetActorLocation(FMath::Lerp(GetActorLocation(), DoorOpenLocation, DoorLerp));
			DoorLerp += 0.25f;
		}
	}
}

