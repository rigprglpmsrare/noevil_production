// Fill out your copyright notice in the Description page of Project Settings.


#include "MazePressurePlate.h"
#include "Components/SphereComponent.h"

AMazePressurePlate::AMazePressurePlate()
	: PlateMesh(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Pressure Plate Mesh")))
	, PlateTrigger(CreateDefaultSubobject<USphereComponent>(TEXT("Puzzle Button Sphere")))
	, PlateMaterial(CreateDefaultSubobject<UMaterial>(TEXT("Pressure Plate Material")))
	, StartLocation(FVector::ZeroVector)
	, SteppedOn(false)
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = PlateTrigger;

	PlateMesh->SetupAttachment(RootComponent);

	PlateTrigger->InitSphereRadius(90.0f);

	bReplicates = true;
	bReplicateMovement = true;
}

void AMazePressurePlate::BeginPlay()
{
	Super::BeginPlay();
	
	StartLocation = GetActorLocation();
}

void AMazePressurePlate::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMazePressurePlate, SteppedOn);
}

void AMazePressurePlate::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SteppedOn)
	{
		SetActorLocation(FMath::Lerp(GetActorLocation(), PressurePlateTarget->GetTargetLocation(), 0.01f));

		if (LinkedPlate->SteppedOn)
		{
			AssignedDoor->SetActorLocation(FMath::Lerp(AssignedDoor->GetActorLocation(), DoorEndTarget->GetActorLocation(), 0.1f));
			if (MazeEntranceDoor)
			{
				FVector EntranceLocation = MazeEntranceDoor->GetActorLocation() + FVector(0.0f, 0.0f, 300.0f);
				MazeEntranceDoor->SetActorLocation(FMath::Lerp(MazeEntranceDoor->GetActorLocation(), EntranceLocation, 0.1f));
			}
		}
	}
	else if (!SteppedOn)
	{
		SetActorLocation(FMath::Lerp(GetActorLocation(), StartLocation, 0.01f));
	}

}

void AMazePressurePlate::Activated()
{
	if (!SteppedOn)
	{
		SteppedOn = true;
	}
}

void AMazePressurePlate::Deactivated()
{
	if (SteppedOn)
	{
		SteppedOn = false;
	}
}

