// Fill out your copyright notice in the Description page of Project Settings.


#include "MazeDoor.h"

AMazeDoor::AMazeDoor()
	: DoorMesh(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh")))
{
	PrimaryActorTick.bCanEverTick = true;
}
