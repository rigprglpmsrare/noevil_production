#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"
#include "Kismet/GameplayStatics.h"
#include "Tests/AutomationCommon.h"
#include "Engine.h"
#include "MovablePuzzleBlock.h"

UWorld* GetTestWorld()
{
	UWorld* TestWorld = nullptr;
	const TIndirectArray<FWorldContext>& WorldContexts = GEngine->GetWorldContexts();
	for (const FWorldContext& Context : WorldContexts)
	{
		if (((Context.WorldType == EWorldType::PIE) || (Context.WorldType == EWorldType::Game)) && (Context.World() != NULL))
		{
			TestWorld = Context.World();
			break;
		}
	}

	return TestWorld;
}

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FWhenAtStartLocationBlockMovesToTarget, "NoEvilTests.UnitTests.MovablePuzzleBlock.WhenAtStartLocationBlockMovesToTarget", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
bool FWhenAtStartLocationBlockMovesToTarget::RunTest(const FString& Parameters)
{
	AutomationOpenMap(TEXT("/Game/Tests/PuzzleBlockTests"));

	UWorld* World = GetTestWorld();
	const FVector SpawnLocation = FVector(1.0f, 1.0f, 1.0f);
	FActorSpawnParameters Params;

	AMovablePuzzleBlock* TestBlock = World->SpawnActor<AMovablePuzzleBlock>(SpawnLocation, FRotator::ZeroRotator, Params);

	TestBlock->TargetLocation = FVector(10.0f, 10.0f, 10.0f);

	TestEqual("Expected puzzle block location to match start location", TestBlock->GetActorLocation(), SpawnLocation);

	TestBlock->ChangeLocation();

	TestTrue("Expecting test block to start moving to target location", TestBlock->MoveToTargetLocation);
	TestFalse("Expecting test block not to move to start location", TestBlock->MoveToStartLocation);

	return true;
}

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FWhenAtTargetLocationBlockMovesToStart, "NoEvilTests.UnitTests.MovablePuzzleBlock.WhenAtTargetLocationBlockMovesToStart", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
bool FWhenAtTargetLocationBlockMovesToStart::RunTest(const FString& Parameters)
{
	AutomationOpenMap(TEXT("/Game/Tests/PuzzleBlockTests"));

	UWorld* World = GetTestWorld();
	const FVector SpawnLocation = FVector(1.0f, 1.0f, 1.0f);
	FActorSpawnParameters Params;

	AMovablePuzzleBlock* TestBlock = World->SpawnActor<AMovablePuzzleBlock>(SpawnLocation, FRotator::ZeroRotator, Params);

	TestBlock->TargetLocation = FVector(10.0f, 10.0f, 10.0f);
	TestBlock->SetActorLocation(TestBlock->TargetLocation);

	TestEqual("Expect block to be at target location", TestBlock->GetActorLocation(), TestBlock->TargetLocation);

	TestBlock->ChangeLocation();

	TestTrue("Expecting test block to start moving to start location", TestBlock->MoveToStartLocation);
	TestFalse("Expecting test block to not move to target location", TestBlock->MoveToTargetLocation);

	return true;
}