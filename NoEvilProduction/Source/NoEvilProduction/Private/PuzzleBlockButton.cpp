// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzleBlockButton.h"
#include "Components/SphereComponent.h"
#include "MovablePuzzleBlock.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APuzzleBlockButton::APuzzleBlockButton()
	: ButtonMesh(nullptr)
	, AssignedPuzzleBlock(nullptr)
{
	
	//RootComponent = ButtonMesh;


	ButtonTrigger = CreateDefaultSubobject<USphereComponent>(TEXT("Puzzle Button Sphere"));
	ButtonTrigger->InitSphereRadius(90.0f);
	//ButtonTrigger->SetupAttachment(RootComponent);
	RootComponent = ButtonTrigger;

	ButtonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Button Mesh"));
	ButtonMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APuzzleBlockButton::BeginPlay()
{
	Super::BeginPlay();
}