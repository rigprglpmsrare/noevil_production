// Fill out your copyright notice in the Description page of Project Settings.


#include "ColourWheel.h"
#include "Materials/Material.h"

// Sets default values
AColourWheel::AColourWheel()
	: ColourWheelMesh(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Colour Wheel Mesh")))
	, ColourOnMaterial(CreateDefaultSubobject<UMaterial>(TEXT("Colour On Material")))
	, ColourOffMaterial(CreateDefaultSubobject<UMaterial>(TEXT("Colour Off Material")))
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = ColourWheelMesh;
}

// Called when the game starts or when spawned
void AColourWheel::BeginPlay()
{
	Super::BeginPlay();
	
	ColourWheelMesh->SetMaterial(0, ColourOnMaterial);
}

void AColourWheel::ChangeToColourOffMaterial()
{
	ColourWheelMesh->SetMaterial(0, ColourOffMaterial);
}

void AColourWheel::ChangeToColourOnMaterial()
{
	ColourWheelMesh->SetMaterial(0, ColourOnMaterial);
}