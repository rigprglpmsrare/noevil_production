// Fill out your copyright notice in the Description page of Project Settings.


#include "MazeMap.h"
#include "Materials/Material.h"
#include "Components/BoxComponent.h"
#include "ColourWheel.h"

// Sets default values
AMazeMap::AMazeMap()
	: MapMesh(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Map Mesh")))
	, ColourOnMat(CreateDefaultSubobject<UMaterial>(TEXT("Colour Module On Display")))
	, ColourOffMat(CreateDefaultSubobject<UMaterial>(TEXT("Colour Module Off Display")))
	, InteractionTrigger(CreateDefaultSubobject<UBoxComponent>(TEXT("Interaction Trigger")))
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = MapMesh;

	InteractionTrigger->InitBoxExtent(FVector(1.0f, 1.0f, 1.0f));
	InteractionTrigger->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AMazeMap::BeginPlay()
{
	Super::BeginPlay();
	
	MapMesh->SetMaterial(0, ColourOnMat);
}

void AMazeMap::ChangeToColourOffMaterial()
{
	MapMesh->SetMaterial(0, ColourOffMat);
	AssignedColourWheel->ChangeToColourOffMaterial();
}

void AMazeMap::ChangeToColourOnMaterial()
{
	MapMesh->SetMaterial(0, ColourOnMat);
	AssignedColourWheel->ChangeToColourOnMaterial();
}
