
#include "MazeExitKey.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

AMazeExitKey::AMazeExitKey()
	: KeyMesh(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Key Mesh")))
	, KeyPickupTrigger(CreateDefaultSubobject<USphereComponent>(TEXT("Key Pickup Area")))
{
	RootComponent = KeyPickupTrigger;
	KeyPickupTrigger->InitSphereRadius(90.0f);

	KeyMesh->SetupAttachment(RootComponent);

	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
}

void AMazeExitKey::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMazeExitKey::PickUp_Implementation()
{
	this->Destroy();
}

void AMazeExitKey::AddToInventory(TArray<FString>& Inventory)
{
	Inventory.Add("ExitKey");

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickupSound, GetActorLocation());
}
