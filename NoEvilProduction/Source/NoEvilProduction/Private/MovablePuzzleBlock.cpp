// Fill out your copyright notice in the Description page of Project Settings.


#include "MovablePuzzleBlock.h"
#include "UnrealNetwork.h"

AMovablePuzzleBlock::AMovablePuzzleBlock()
	: BlockMesh(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Block Mesh")))
	, StartingLocation(FVector::ZeroVector)
	, TargetLocation(FVector::ZeroVector)
	, MoveToTargetLocation(false)
	, MoveToStartLocation(false)
	, RateOfInterpolation(0.01f)
	, LocationCheckTolerance(5.0f)
	, CurrentLocation(FVector::ZeroVector)
{

	PrimaryActorTick.bCanEverTick = true;

	BlockMesh->SetupAttachment(RootComponent);

	bReplicates = true;
	bReplicateMovement = true;
}

void AMovablePuzzleBlock::BeginPlay()
{
	Super::BeginPlay();

	StartingLocation = GetActorLocation();
	
	if (TargetLocationActor)
	{
		TargetLocation = TargetLocationActor->GetActorLocation();
	}
	else
	{
		//Debug message
	}
}

void AMovablePuzzleBlock::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMovablePuzzleBlock, MoveToTargetLocation);
	DOREPLIFETIME(AMovablePuzzleBlock, MoveToStartLocation);
}

void AMovablePuzzleBlock::Tick(float DeltaTime)
{
	if (MoveToTargetLocation)
	{
		CurrentLocation = GetActorLocation();
		SetActorLocation(FMath::Lerp(CurrentLocation, TargetLocation, RateOfInterpolation));
		CurrentLocation = GetActorLocation();
		if (CurrentLocation.Equals(TargetLocation, 1.0f))
		{
			MoveToTargetLocation = false;
		}
	}

	else if (MoveToStartLocation)
	{
		CurrentLocation = GetActorLocation();
		SetActorLocation(FMath::Lerp(CurrentLocation, StartingLocation, RateOfInterpolation));
		CurrentLocation = GetActorLocation();

		if (CurrentLocation.Equals(StartingLocation, 1.0f))
		{
			MoveToStartLocation = false;
		}
	}
}

void AMovablePuzzleBlock::ChangeLocation_Implementation()
{
	CurrentLocation = GetActorLocation();

	if (CurrentLocation.Equals(StartingLocation, LocationCheckTolerance))
	{
		MoveToTargetLocation = true;
	}
	else if (CurrentLocation.Equals(TargetLocation, LocationCheckTolerance))
	{
		MoveToStartLocation = true;
	}
}