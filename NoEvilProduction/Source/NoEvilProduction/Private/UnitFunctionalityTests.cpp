// Fill out your copyright notice in the Description page of Project Settings.


#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FTestPassesWhenBoolTrue, "NoEvilTests.UnitTests.TestInfrastructure.TestPassesWhenBoolTrue", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
bool FTestPassesWhenBoolTrue::RunTest(const FString& Parameters)
{
	bool ShouldTestPass = false;

	TestFalse("ShouldTestPass bool expected to be false at the start", ShouldTestPass);

	ShouldTestPass = true;

	TestTrue("ShouldTestPass bool expected to be true at end of test", ShouldTestPass);

	return true;
}

