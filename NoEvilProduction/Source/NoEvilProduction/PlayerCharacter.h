// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "MovablePuzzleBlock.h"
#include "MazeExitDoor.h"
#include "PlayerCharacter.generated.h"

class APuzzleBlockButton;
class USphereComponent;
class AMazeExitKey;
class AMazeMap;

UCLASS()
class NOEVILPRODUCTION_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, Category = "Modules")
		float PlayerFocalDistance;

	UPROPERTY(EditDefaultsOnly, Category = "Modules")
		float ScreenVignetteIntensity;

	TArray<FString> PlayerInventory;

private:

	bool ColourModuleOn;
	bool VisionModuleOn;
	bool MazeEntranceEffect;
	bool MazeEffectTrigger;

	UFUNCTION()
		void MoveForward(float ScaleValue);

	UFUNCTION()
		void MoveRight(float ScaleValue);

	UFUNCTION(BlueprintCallable, Category = "Modules", Client, Reliable)
		void ColourModuleToggle();

	UFUNCTION(BlueprintCallable, Category = "Modules", Client, Reliable)
		void VisionModuleToggle();

	UFUNCTION(Client, Reliable)
		void MazeEntranceEffectToggle();

	UFUNCTION(Server, Reliable)
		void AvailableInteractions();

	UFUNCTION(Client, Reliable)
		void AddKeyToInventory();

	UFUNCTION(Client, Reliable)
		void ButtonPressSound();

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* PlayerCamera;

	UPROPERTY(VisibleAnywhere, Replicated)
	APuzzleBlockButton* ButtonAvailableToPress;

	UPROPERTY(VisibleAnywhere, Replicated)
	AMazeExitKey* KeyAvailableToPickup;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(VisibleAnywhere)
	USphereComponent* InteractableSphere;

	UFUNCTION(Server, Reliable)
	void MazeRestart();

	UPROPERTY(EditAnywhere)
		AActor* MazeRestartLocation;

	UPROPERTY(EditAnywhere)
	AMazeExitDoor* MazeExitDoor;

	UPROPERTY(EditAnywhere)
		AMazeMap* MazeNavigationMap;
};
