// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MazeDoor.generated.h"

UCLASS()
class NOEVILPRODUCTION_API AMazeDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMazeDoor();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* DoorMesh;

};
