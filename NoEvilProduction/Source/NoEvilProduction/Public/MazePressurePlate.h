// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/Material.h"
#include "MazeDoor.h"
#include "UnrealNetwork.h"
#include "MazePressurePlate.generated.h"

class USphereComponent;

UCLASS()
class NOEVILPRODUCTION_API AMazePressurePlate : public AActor
{
	GENERATED_BODY()
	
public:	
	AMazePressurePlate();

	virtual void Tick(float DeltaTime) override;

	void Activated();
	void Deactivated();

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, Replicated)
		bool SteppedOn;

	FVector StartLocation;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlateMesh;

	UPROPERTY(VisibleAnywhere)
		USphereComponent* PlateTrigger;

	UPROPERTY(EditAnywhere)
		UMaterial* PlateMaterial;

	UPROPERTY(EditAnywhere)
		AMazeDoor* AssignedDoor;
	UPROPERTY(EditAnywhere)
		AMazeDoor* MazeEntranceDoor;

	UPROPERTY(EditAnywhere)
		AMazePressurePlate* LinkedPlate;

	UPROPERTY(EditAnywhere)
		AActor* PressurePlateTarget;

	UPROPERTY(EditAnywhere)
		AActor* DoorEndTarget;
};
