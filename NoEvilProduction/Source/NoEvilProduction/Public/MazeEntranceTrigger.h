// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "MazeEntranceTrigger.generated.h"

/**
 * 
 */
UCLASS()
class NOEVILPRODUCTION_API AMazeEntranceTrigger : public ATriggerBox
{
	GENERATED_BODY()
};
