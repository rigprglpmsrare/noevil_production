// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MazeMap.generated.h"

class UMaterial;
class UBoxComponent;
class AColourWheel;

UCLASS()
class NOEVILPRODUCTION_API AMazeMap : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMazeMap();

	void ChangeToColourOffMaterial();

	void ChangeToColourOnMaterial();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MapMesh;

	UPROPERTY(EditAnywhere)
	UMaterial* ColourOnMat;

	UPROPERTY(EditAnywhere)
	UMaterial* ColourOffMat;

	UPROPERTY(EditAnywhere)
	UBoxComponent* InteractionTrigger;

	UPROPERTY(EditAnywhere)
	AColourWheel* AssignedColourWheel;
};
