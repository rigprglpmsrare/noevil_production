// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "MazeRestartTrigger.generated.h"

/**
 * 
 */
UCLASS()
class NOEVILPRODUCTION_API AMazeRestartTrigger : public ATriggerBox
{
	GENERATED_BODY()
	
};
