// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MazeDoor.h"
#include "MazeExitDoor.generated.h"

class UBoxComponent;

UCLASS()
class NOEVILPRODUCTION_API AMazeExitDoor : public AMazeDoor
{
	GENERATED_BODY()

public:
	AMazeExitDoor();

	void ShouldOpen();

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;
	
private:
	UPROPERTY(EditAnywhere)
	UBoxComponent* ExitDoorTrigger;

	bool IsOpening;

	UPROPERTY(EditAnywhere)
	AActor* DoorOpenActor;

	UPROPERTY()
	FVector DoorOpenLocation;
};
