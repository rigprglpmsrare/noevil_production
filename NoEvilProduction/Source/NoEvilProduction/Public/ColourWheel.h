// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ColourWheel.generated.h"

class UMaterial;

UCLASS()
class NOEVILPRODUCTION_API AColourWheel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AColourWheel();

	void ChangeToColourOffMaterial();

	void ChangeToColourOnMaterial();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* ColourWheelMesh;

	UPROPERTY(EditAnywhere)
	UMaterial* ColourOnMaterial;

	UPROPERTY(EditAnywhere)
	UMaterial* ColourOffMaterial;
};
