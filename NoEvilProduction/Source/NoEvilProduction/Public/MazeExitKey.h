
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MazeDoor.h"
#include "MazeExitKey.generated.h"

class USphereComponent;
class USoundWave;

UCLASS()
class NOEVILPRODUCTION_API AMazeExitKey : public AActor
{
	GENERATED_BODY()
	
public:	
	AMazeExitKey();

	UFUNCTION(Server, Reliable)
	void PickUp();

	void AddToInventory(TArray<FString>& Inventory);

protected:
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* KeyMesh;

	UPROPERTY(EditAnywhere)
	USphereComponent* KeyPickupTrigger;

	UPROPERTY(EditAnywhere)
	USoundWave* PickupSound;
};
