// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PuzzleBlockButton.generated.h"

class AMovablePuzzleBlock;
class USphereComponent;
class USoundWave;

UCLASS()
class NOEVILPRODUCTION_API APuzzleBlockButton : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APuzzleBlockButton();

	UPROPERTY(EditAnywhere)
		USoundWave* PressedSound;

	UPROPERTY(EditAnywhere)
		AMovablePuzzleBlock* AssignedPuzzleBlock;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	UPROPERTY(VisibleAnywhere)
	USphereComponent* ButtonTrigger;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* ButtonMesh;


};
