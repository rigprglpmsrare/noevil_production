// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MovablePuzzleBlock.generated.h"

UCLASS()
class NOEVILPRODUCTION_API AMovablePuzzleBlock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMovablePuzzleBlock();

	UPROPERTY(VisibleAnywhere, Replicated)
		bool MoveToTargetLocation;

	UPROPERTY(VisibleAnywhere, Replicated)
		bool MoveToStartLocation;

	UPROPERTY(VisibleAnywhere)
		FVector TargetLocation;

	UPROPERTY(EditAnywhere)
		FVector StartingLocation;

	UFUNCTION(Server, Reliable)
	void ChangeLocation();

	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	float RateOfInterpolation;
	float LocationCheckTolerance;

	FVector CurrentLocation;

	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* BlockMesh;

	UPROPERTY(EditAnywhere)
		AActor* TargetLocationActor;

	UPROPERTY(EditAnywhere)
		AActor* StartLocationActor;
};
